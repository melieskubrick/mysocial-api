# My Social Api
![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)
![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white)
![Express](https://img.shields.io/badge/express-%23323330?style=for-the-badge&logo=express&logoColor=white)
![MongoDB](https://img.shields.io/badge/mongodb-6DA55F?style=for-the-badge&logo=mongodb&logoColor=white)

# Doc
  - [1. Introdução](#1-introdução)
  - [2. Projetos](#2-projetos)
    - [2.1 Aplicação back-end (API)](#21-aplicação-back-end)
    - [2.2 Aplicação front-end (mobile)](#22-aplicação-front-end-mobile)
  - [3. Layout](#3-layout)
  - [4. Pré-requisitos](#4-pré-requisitos)
    - [4.1 Clonar repositório](#41-clonar-repositório)
    - [4.2 Pacotes](#42-pacotes)
  - [5. Execução](#5-execução)
  
## 1. Introdução[](url)
MySocial é uma rede social onde você pode interagir com sua comunidade sobre diversos assuntos.

## 2. Projetos

* ### 2.1 Aplicação back-end
API utilizando [Node.js](https://nodejs.org/) e [MongoDB](https://www.mongodb.com/) que retorna um JSON com as informações dos usuários para uso pelas aplicações front-end.

* ###  2.2 Aplicação front-end mobile
Aplicação para celular utilizando [React Native](https://reactnative.dev/) acessando a API do back-end e apresentando os dados no dispositivo móvel. [Link do repositório do Front-end mobile](https://gitlab.com/melieskubrick/mysocial)

## 3. Layout
O layout do projeto pode ser visualizado no link [design do projeto](https://www.figma.com/file/H95mB31PtdN085k03rXNFH/MySocial?type=design&node-id=0%3A1&mode=design&t=tDU7zc7FnUJhzABi-1). É necessário ter conta no [Figma](https://figma.com) para acessá-lo.

## 4. Pré-requisitos

### 4.1 Clonar repositório
> Clonar o repositório [Gitlab]
```sh
   git clone https://gitlab.com/melieskubrick/mysocial-api
   ```  
### 4.2 Pacotes
> Após clonar o projeto por completo rode os comandos abaixo
   ```sh
yarn
   ```
   ```sh
npm install
   ```

## 5. Execução
Para rodar a aplicação segue os comandos
   ```sh
   yarn start
   ```
   ou
   ```sh
   npm run start
   ```

## Libs Utilizadas
Aqui estão os pacotes que foram utilizados para a construção deste app!

* [bcryptjs](https://github.com/dcodeIO/bcrypt.js#readme)
<br/>Algoritimo de criptografia hash
* [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken#readme)
<br/>Os JSON Web Tokens são um método RFC 7519 padrão do setor aberto  para representar declarações de forma segura entre duas partes.
* [mongoose](https://mongoosejs.com/)
<br/>Modelagem elegante de objetos mongodb para node.js
* [nodemailer](https://nodemailer.com/)
<br/>O Nodemailer é um módulo para aplicativos Node.js para permitir o envio de e-mail fácil
* [yup](https://github.com/jquense/yup)
<br/>Yup é um construtor de esquema para análise e validação de valor de tempo de execução

Made with ♥
