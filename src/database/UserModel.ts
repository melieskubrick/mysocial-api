// @ts-nocheck
import { Schema } from "mongoose";
import mongoose from "mongoose";

var bcrypt = require('bcryptjs');

const UserModel = new Schema(
  {
    name: {
      type: String,
      require: true,
    },
    email: {
      type: String,
      unique: true,
      require: true,
      lowercase: true,
    },
    password: {
      type: String,
      require: true,
      select: false,
    },
    passwordResetToken: {
      type: String,
      select: false,
    },
    passwordResetExpires: {
      type: Date,
      select: false,
    },
    active: {
      type: Boolean,
      default: false,
    },
    otp: {
      type: String,
      select: false,
    },
  },
  { timestamps: true }
);

UserModel.pre("save", async function (next) {
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(this.password, salt);

  this.password = hashedPassword;
  next();
});

export default mongoose.model("User", UserModel);
