// @ts-nocheck
import { Request, Response } from "express";
import UserModel from "../database/UserModel";

const { generateOTP } = require('../otp');

const { sendMail } = require('../emailService');

import jwt from "jsonwebtoken";
import crypto from "crypto";

var bcrypt = require('bcryptjs');
const authConfig = require("../config/auth.json");
const mailer = require("../modules/mailer");

const generateToken = (params = {}) => {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
};

const validateUserSignUp = async (email, otp) => {
  const user = await UserModel.findOne({ email }).select("+otp");
  console.log("userOTP", user)
  console.log("OTP", otp)
  if (!user) {
    return [false, 'User not found'];
  }
  if (user && user.otp !== otp) {
    return [false, 'Invalid OTP'];
  }
  const updatedUser = await UserModel.findByIdAndUpdate(user._id, {
    $set: { active: true },
  });
  return [true, updatedUser];
};

const AuthController = {
  async verify(req: Request, res: Response): Promise<Response> {
    const { email, otp } = req.body;
    const user = await validateUserSignUp(email, otp);
    res.send(user);
  },
  async create(req: Request, res: Response): Promise<Response> {
    const { email } = req.body;

    try {
      if (await UserModel.findOne({ email }))
        return res
          .status(400)
          .json({ error: true, message: "Email já resgitrado" });

      const otpGenerated = generateOTP();
      const user = await UserModel.create({ ...req.body, otp: otpGenerated });

      user.password = undefined;
      user.otp = undefined;

      await sendMail({
        to: email,
        OTP: otpGenerated,
      });
      return res.json({ user, token: generateToken({ id: user.id }) });
    } catch (err: any) {
      return res.status(400).json({
        error: true,
        message: "Falha no cadastro",
        errMessage: err.message,
      });
    }
  },
  async login(req: Request, res: Response): Promise<Response> {
    const { email, password } = req.body;
    const user = await UserModel.findOne({ email }).select("+password");

    if (!user)
      return res
        .status(400)
        .json({ error: true, message: "Usuário não encontrado" });

    if (!(bcrypt.compareSync(password, user.password)))
      return res.status(400).json({ error: true, message: "Senha inválida" });

    user.password = undefined;

    return res.json({ user, token: generateToken({ id: user.id }) });
  },
  async forgot(req: Request, res: Response): Promise<Response> {
    const { email } = req.body;

    try {
      const user = UserModel.findOne({ email });

      if (!user)
        return res
          .status(400)
          .json({ error: true, message: "Usuário não encontrado" });

      const token = crypto.randomBytes(20).toString("hex");

      const now = new Date();
      now.setHours(now.getHours() + 1);

      await UserModel.findByIdAndUpdate(user.id, {
        $set: {
          passwordResetToken: token,
          passwordResetExpires: now,
        },
      });

      mailer.sendMail(
        {
          to: email,
          from: "melieskubrick@gmail.com",
          template: "auth/forgot_password",
          context: { token },
        },
        (err: any) => {
          if (err)
            return res.status(400).json({
              error: true,
              message: "Não foi possível enviar o e-mail",
              err: err.message,
            });

          return res.json();
        }
      );
      return res.json();
    } catch (err: any) {
      return res.status(400).json({
        error: true,
        message: "Erro no 'esqueci minha senha', tente novamente",
        errMessage: err.message,
      });
    }
  },
  async reset(req: Request, res: Response): Promise<Response> {
    const { email, token, password } = req.body;
    try {
      const user = await UserModel.findOne({ email }).select(
        "+passwordResetToken passwordResetExpires"
      );

      console.log("USER::", user);

      if (!user)
        return res
          .status(400)
          .json({ error: true, message: "Usuário não encontrado" });

      if (token !== user.passwordResetToken)
        if (!user)
          return res
            .status(400)
            .json({ error: true, message: "Token inválido" });

      const now = new Date();

      if (now > user.passwordResetExpires) {
        return res
          .status(400)
          .json({ error: true, message: "Token expirado, gere um novo" });
      }

      user.password = password;

      await user.save();

      return res.json();
    } catch (err: any) {
      return res.status(400).json({
        error: true,
        message: "Não foi possível resetar a senha, tente novamente",
      });
    }
  },
};

export default AuthController;