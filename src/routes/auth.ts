import { Router } from "express";
import AuthController from "../controllers/AuthController";

const router = Router();

router.post("/auth/register", AuthController.create);
router.post("/auth/authenticate", AuthController.login);
router.post("/auth/forgot_password", AuthController.forgot);
router.post("/auth/reset_password", AuthController.reset);
router.post("/auth/verify", AuthController.verify);

export default router;
