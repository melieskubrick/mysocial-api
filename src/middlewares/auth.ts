// @ts-nocheck
import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
const authConfig = require("../config/auth.json");

module.exports = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  if (!authHeader)
    return res
      .status(401)
      .json({ error: true, message: "O token não foi informado" });

  const parts = authHeader.split(" ");

  if (!parts.length === 2)
    return res.status(401).json({ error: true, message: "Erro no token" });

  const [scheme, token] = parts;

  if (!/^Bearer$/i.test(scheme))
    return res.status(401).json({ error: true, message: "Token malformatted" });

  jwt.verify(token, authConfig.secret, (err: any, decoded: any) => {
    if (err)
      return res.status(401).json({ error: true, message: "Token inválido" });

    req.userId = decoded.id;
    return next();
  });
};
