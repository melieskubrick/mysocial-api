import express from "express";
import mongoose from "mongoose";
import routerAuth from "./routes/auth";
import bodyParser from "body-parser";
import * as dotenv from "dotenv";
dotenv.config();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(routerAuth);

mongoose
  .connect(`${process.env.MONGO_URL}`)
  .then((data) => {
    console.log("MongoDB Connected");
  })
  .catch((err) => {
    console.log("MongoDB error in connection", err.message);
  });

app.listen(process.env.PORT || 3333);
