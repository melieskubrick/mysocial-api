"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UserModel_1 = __importDefault(require("../database/UserModel"));
const { generateOTP } = require('../otp');
const { sendMail } = require('../emailService');
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const crypto_1 = __importDefault(require("crypto"));
var bcrypt = require('bcryptjs');
const authConfig = require("../config/auth.json");
const mailer = require("../modules/mailer");
const generateToken = (params = {}) => {
    return jsonwebtoken_1.default.sign(params, authConfig.secret, {
        expiresIn: 86400,
    });
};
const validateUserSignUp = (email, otp) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield UserModel_1.default.findOne({ email }).select("+otp");
    console.log("userOTP", user);
    console.log("OTP", otp);
    if (!user) {
        return [false, 'User not found'];
    }
    if (user && user.otp !== otp) {
        return [false, 'Invalid OTP'];
    }
    const updatedUser = yield UserModel_1.default.findByIdAndUpdate(user._id, {
        $set: { active: true },
    });
    return [true, updatedUser];
});
const AuthController = {
    verify(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, otp } = req.body;
            const user = yield validateUserSignUp(email, otp);
            res.send(user);
        });
    },
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email } = req.body;
            try {
                if (yield UserModel_1.default.findOne({ email }))
                    return res
                        .status(400)
                        .json({ error: true, message: "Email já resgitrado" });
                const otpGenerated = generateOTP();
                const user = yield UserModel_1.default.create(Object.assign(Object.assign({}, req.body), { otp: otpGenerated }));
                user.password = undefined;
                user.otp = undefined;
                yield sendMail({
                    to: email,
                    OTP: otpGenerated,
                });
                return res.json({ user, token: generateToken({ id: user.id }) });
            }
            catch (err) {
                return res.status(400).json({
                    error: true,
                    message: "Falha no cadastro",
                    errMessage: err.message,
                });
            }
        });
    },
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = req.body;
            const user = yield UserModel_1.default.findOne({ email }).select("+password");
            if (!user)
                return res
                    .status(400)
                    .json({ error: true, message: "Usuário não encontrado" });
            if (!(bcrypt.compareSync(password, user.password)))
                return res.status(400).json({ error: true, message: "Senha inválida" });
            user.password = undefined;
            return res.json({ user, token: generateToken({ id: user.id }) });
        });
    },
    forgot(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email } = req.body;
            try {
                const user = UserModel_1.default.findOne({ email });
                if (!user)
                    return res
                        .status(400)
                        .json({ error: true, message: "Usuário não encontrado" });
                const token = crypto_1.default.randomBytes(20).toString("hex");
                const now = new Date();
                now.setHours(now.getHours() + 1);
                yield UserModel_1.default.findByIdAndUpdate(user.id, {
                    $set: {
                        passwordResetToken: token,
                        passwordResetExpires: now,
                    },
                });
                mailer.sendMail({
                    to: email,
                    from: "melieskubrick@gmail.com",
                    template: "auth/forgot_password",
                    context: { token },
                }, (err) => {
                    if (err)
                        return res.status(400).json({
                            error: true,
                            message: "Não foi possível enviar o e-mail",
                            err: err.message,
                        });
                    return res.json();
                });
                return res.json();
            }
            catch (err) {
                return res.status(400).json({
                    error: true,
                    message: "Erro no 'esqueci minha senha', tente novamente",
                    errMessage: err.message,
                });
            }
        });
    },
    reset(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, token, password } = req.body;
            try {
                const user = yield UserModel_1.default.findOne({ email }).select("+passwordResetToken passwordResetExpires");
                console.log("USER::", user);
                if (!user)
                    return res
                        .status(400)
                        .json({ error: true, message: "Usuário não encontrado" });
                if (token !== user.passwordResetToken)
                    if (!user)
                        return res
                            .status(400)
                            .json({ error: true, message: "Token inválido" });
                const now = new Date();
                if (now > user.passwordResetExpires) {
                    return res
                        .status(400)
                        .json({ error: true, message: "Token expirado, gere um novo" });
                }
                user.password = password;
                yield user.save();
                return res.json();
            }
            catch (err) {
                return res.status(400).json({
                    error: true,
                    message: "Não foi possível resetar a senha, tente novamente",
                });
            }
        });
    },
};
exports.default = AuthController;
