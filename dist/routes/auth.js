"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const AuthController_1 = __importDefault(require("../controllers/AuthController"));
const router = (0, express_1.Router)();
router.post("/auth/register", AuthController_1.default.create);
router.post("/auth/authenticate", AuthController_1.default.login);
router.post("/auth/forgot_password", AuthController_1.default.forgot);
router.post("/auth/reset_password", AuthController_1.default.reset);
router.post("/auth/verify", AuthController_1.default.verify);
exports.default = router;
