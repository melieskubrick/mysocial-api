"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const MAIL_SETTINGS = {
    service: 'gmail',
    providerauth: { user: process.env.MAIL_EMAIL },
    pass: process.env.MAIL_PASSWORD
};
const nodemailer = require('nodemailer');
module.exports.sendMail = (params) => __awaiter(void 0, void 0, void 0, function* () {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: process.env.MAIL_EMAIL,
            pass: process.env.MAIL_PASSWORD,
            clientId: process.env.CLIENT_ID,
            clientSecret: process.env.CLIENT_SECRET,
            refreshToken: process.env.REFRESH_TOKEN
        }
    });
    try {
        let info = yield transporter.sendMail({
            from: MAIL_SETTINGS.providerauth.user,
            to: params.to,
            subject: 'Ola',
            html: `
        <div
          class="container"
          style="max-width: 90%; margin: auto; padding-top: 20px"
        >
          <h2>Seja bem vindo(a)</h2>
          <h4>Siga os proximos passos:</h4>
          <p style="margin-bottom: 30px;">Para finalizar o seu cadastro use o código abaixo no aplicativo MySocial</p>
          <h1 style="font-size: 40px; letter-spacing: 2px; text-align:center;">${params.OTP}</h1>
     </div>
      `,
        });
        return info;
    }
    catch (error) {
        console.log(error);
        return false;
    }
});
