"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const authConfig = require("../config/auth.json");
module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader)
        return res
            .status(401)
            .json({ error: true, message: "O token não foi informado" });
    const parts = authHeader.split(" ");
    if (!parts.length === 2)
        return res.status(401).json({ error: true, message: "Erro no token" });
    const [scheme, token] = parts;
    if (!/^Bearer$/i.test(scheme))
        return res.status(401).json({ error: true, message: "Token malformatted" });
    jsonwebtoken_1.default.verify(token, authConfig.secret, (err, decoded) => {
        if (err)
            return res.status(401).json({ error: true, message: "Token inválido" });
        req.userId = decoded.id;
        return next();
    });
};
